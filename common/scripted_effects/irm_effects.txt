# IRM: Imperial Routine Mod
# Author: Iyur
# Mod custom effects

# ANY # -------------------------------------------------------------------------------------------

# This = Any
# Initializes all mod components.
irm_init_modules = {
	# base
	if = {
		limit = {
			not = { has_global_flag = irm_on }
		}
		set_global_flag = irm_on
		log = "IRM: on"
	}
	# system/sector wide effects
	if = {
		limit = {
			nor = {
				has_global_flag = irm_effect_providers_on
				has_global_flag = irm_effect_providers_off
			}
		}
		irm_enable_effect_providers = yes
	}
	# system leaders
	if = {
		limit = {
			nor = {
				has_global_flag = irm_system_leaders_on
				has_global_flag = irm_system_leaders_off
			}
		}
		irm_enable_system_leaders = yes
	}
	# sector differences
	if = {
		limit = {
			nor = {
				has_global_flag = irm_sector_diffs_on
				has_global_flag = irm_sector_diffs_off
			}
		}
		irm_enable_sector_diffs = yes
	}
	# sector shape
	if = {
		limit = {
			nor = {
				has_global_flag = irm_sector_shape_on
				has_global_flag = irm_sector_shape_off
			}
		}
		irm_enable_sector_shape = yes
	}	
}

#	^
	# This = Country
	# Adds/removes special deposit as a core effect provider
	irm_enable_effect_providers = {
		remove_global_flag = irm_effect_providers_off
		set_global_flag = irm_effect_providers_on
		every_country = {
			limit = { is_ai = no }
				every_owned_planet = {
					limit = { not = { has_deposit = irm_colonial_center } }
						add_deposit = irm_colonial_center
				}
		}
		log = "IRM: effect_providers enabled"	
	}
	irm_disable_effect_providers = {
		remove_global_flag = irm_effect_providers_on
		set_global_flag = irm_effect_providers_off	
		every_country = {
			limit = { is_country_type = default }
				every_owned_planet = {
					limit = { has_deposit = irm_colonial_center }
						remove_deposit = irm_colonial_center
				}
		}
		log = "IRM: effect_providers disabled"
		# dependent modules
		irm_disable_system_leaders = yes
		irm_disable_sector_diffs = yes
	}

#	^ This = Country
	# On/off all system leaders, unassigns them from starbases
	irm_enable_system_leaders = {
		remove_global_flag = irm_system_leaders_off
		set_global_flag = irm_system_leaders_on
		log = "IRM: system_leaders enabled"	
	}	
	irm_disable_system_leaders = {
		remove_global_flag = irm_system_leaders_on
		set_global_flag = irm_system_leaders_off
		every_country = {
			limit = { is_ai = no }
				every_owned_starbase = {
					limit = { exists = fleet.leader }
						fleet.leader = { unassign_leader = this }
				}
		}
		log = "IRM: system_leaders disabled"	
	}	

#	^ This = Country
	# On/off all sector differences, based on their types
	irm_enable_sector_diffs = {
		remove_global_flag = irm_sector_diffs_off
		set_global_flag = irm_sector_diffs_on
		log = "IRM: sector_diffs enabled"	
	}
	irm_disable_sector_diffs = {
		remove_global_flag = irm_sector_diffs_on
		set_global_flag = irm_sector_diffs_off
		log = "IRM: sector_diffs disabled"	
	}

#	^ This = Country
	# On/off sector shape features
	irm_enable_sector_shape = {
		remove_global_flag = irm_sector_shape_off
		set_global_flag = irm_sector_shape_on
		log = "IRM: sector_shape enabled"	
	}
	irm_disable_sector_shape = {
		remove_global_flag = irm_sector_shape_on
		set_global_flag = irm_sector_shape_off
		log = "IRM: sector_shape disabled"	
	}	


# This = Any
# $stTarget$ : string : name of the target scope
# Clears chosen global target
irm_clear_target = {
	if = { # system scope
		limit = { exists = event_target:$stTarget$ }
			clear_global_event_target = $stTarget$
	}	
}

# This = Any
# $stDummy$ : string : name of the target scope
# Physically deletes dummy scope
irm_delete_dummy = {
	if = {
		limit = { exists = event_target:$stDummy$ }
			event_target:$stDummy$ = { kill_leader = { show_notification = no } }
	}
}